package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class WordScanner {

	public WordScanner(String fileName) throws FileNotFoundException {
		
		int aliceTotalWords = 0;
		int aliceTotalLines = 0;
		int aliceTotalSentences = 0;
		
		//(BufferedReader reader = new BufferedReader(new FileReader(fileName))) 
		try {
			aliceLog("header");
			ClassLoader classLoader = getClass().getClassLoader();
			File aliceFile = new File(classLoader.getResource(fileName).getFile());
			FileReader fileReader = new FileReader(aliceFile);
			BufferedReader reader = new BufferedReader(fileReader);
			
			String wordLine = reader.readLine();
			
			while (wordLine != null) {
				aliceTotalLines++;

				String[] myWords = wordLine.split(" ");
				for(int i = 0; i < myWords.length; i++) {
					String word = myWords[i];
					if(word.contains("!") || word.contains("?") || word.contains(".")) {
	                    aliceTotalSentences++;
	                }
				aliceTotalWords += myWords.length;
				}
				
				wordLine = reader.readLine();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		aliceLog("* Total Words: " + aliceTotalWords);
		aliceLog("* Total Sentences: " + aliceTotalSentences);
	}
	
	private static void aliceLog(String string) {
		System.out.println(string);
	}		
}
