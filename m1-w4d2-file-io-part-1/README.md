# File I/O Part 1 Exercises (Pair)

## Word Count

Write a program that, given a filesystem path for a text file, reads the contents of the file (use "Alice's Adventures in Wonderland" as a test input) and displays both the number of words and the number of sentences in the file.

* Hint 1: words are delimited by space characters
* Hint 2: sentences are terminated by either a period, an exclamation mark, or a question mark.

The program is to invoked like so:

```
java com.techelevator.WordCount ./alices_adventures_in_wonderland.txt
```
