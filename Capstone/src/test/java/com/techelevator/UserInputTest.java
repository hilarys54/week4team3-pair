package com.techelevator;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class UserInputTest {
	
	VendingMachine vendingMachine;
	DollarAmount totalMoney;
	

	@Before
	public void setup() {
		Item item = new Item("A1", "Potato Crisps", totalMoney, 5);
		DollarAmount totalMoney = new DollarAmount(325);
		VendingMachine vendingMachine = new VendingMachine();
	}
	
	@Test 
	public void additional_balance_equals_100() {
		DollarAmount additionalBalance = new DollarAmount(100);
		DollarAmount userAmount = UserInput.moneyFeed("1");
		Assert.assertEquals(additionalBalance, userAmount) ;
	}
	
	@Test 
	public void additional_balance_equals_200() {
		DollarAmount additionalBalance = new DollarAmount(200);
		DollarAmount userAmount = UserInput.moneyFeed("2");
		Assert.assertEquals(additionalBalance, userAmount) ;
	}
	
	@Test 
	public void additional_balance_equals_500() {
		DollarAmount additionalBalance = new DollarAmount(500);
		DollarAmount userAmount = UserInput.moneyFeed("5");
		Assert.assertEquals(additionalBalance, userAmount) ;
	}
	
	@Test 
	public void additional_balance_equals_1000() {
		DollarAmount additionalBalance = new DollarAmount(1000);
		DollarAmount userAmount = UserInput.moneyFeed("10");
		Assert.assertEquals(additionalBalance, userAmount) ;
	}
	
	@Test
	public void additional_balance_is_not_the_same() {
		DollarAmount additionalBalance = new DollarAmount(0);
		DollarAmount userAmount = UserInput.moneyFeed("7");
		Assert.assertEquals(additionalBalance, userAmount) ;	
	}
	@Test
	public void moneyfeed_test() {
		String valueOfBills = "5";
		DollarAmount additonalBalance = new DollarAmount(0);
		DollarAmount totalMoney = additonalBalance.plus(new DollarAmount(500));
		Assert.assertEquals("$5.00", totalMoney.toString()) ;
	}
	@Test
	public void while_running_test() {
		boolean running = true;
		Assert.assertTrue(running);
	}
	


}
