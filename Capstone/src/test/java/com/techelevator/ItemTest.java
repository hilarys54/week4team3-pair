package com.techelevator;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	Item itemTest;
	DollarAmount dollarTest;

	@Before
	public void setUp() {
		dollarTest = new DollarAmount(325);
		itemTest = new Item("A1", "Potato Crisps", dollarTest, 5); 
	}

	@Test
	public void test_string_amount_format() {
		dollarTest.hashCode();
	String slotId = itemTest.getSlotIdentifier();
	String name = itemTest.getItemName();
	DollarAmount dollar = itemTest.getItemPrice();
	int quantity = itemTest.getQuantity();
	
	Assert.assertEquals("A1", slotId);
	Assert.assertEquals("Potato Crisps", name);
	Assert.assertEquals(dollarTest, dollar);
	Assert.assertEquals(5, quantity);
	}
}
