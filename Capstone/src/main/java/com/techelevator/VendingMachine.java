package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class VendingMachine {
	
//	int inventory = 5;
	DollarAmount totalMoney;
	HashMap<String,Item> itemMap = new HashMap<String, Item>();
	//List<Item> itemList = new ArrayList<Item>();
	
	
	
	public VendingMachine() {
		BufferedReader reader =null;
		Scanner scanner = null;
		try {
			
			FileInputStream newInput = new FileInputStream("./vendingmachine.csv");
			Scanner inputScanner = new Scanner(newInput);
	   
			//String[] parts = line.split("\\|");
			while(inputScanner.hasNext()) {
				String line = inputScanner.nextLine();
				String slot[] = line.split("\\|");
				double price = Double.parseDouble(slot[2]);
				int priceInPennies = (int)(price * 100);
				
		        Item item = new Item(slot[0], slot[1], new DollarAmount(priceInPennies), 5);
	            itemMap.put(slot[0], item);
	            //itemList.add(item);
			        
	  	
			}
			

		} catch (IOException e) {
		
			e.printStackTrace();
		
		} finally {
			if(reader != null) {
		
			}if(scanner !=null) {
				scanner.close();
			}
}
}
//	public Item getItems(String itemKey, Item item) {
//		return itemMap.get(itemKey);
//	}
	public Item getItem(String itemKey) {
		return itemMap.get(itemKey);
	}

	public Collection<Item> getItems(){
		return itemMap.values();
	}
}
