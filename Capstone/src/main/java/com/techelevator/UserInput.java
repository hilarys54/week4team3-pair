package com.techelevator;

import java.awt.List;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.plaf.SliderUI;

public class UserInput {


  
	// protected HashMap<String , Item> itemMap ;
    private static VendingMachine vendingMachine ;

	   
  // protected static DollarAmount totalMoney;
	public static void main(String[] args) {
		vendingMachine = new VendingMachine();
	
		DollarAmount totalMoney = new DollarAmount(0);
		boolean running = true;
		//boolean purchasing = true;
		while (running) { 
			Scanner input = new Scanner(System.in);
			System.out.println("Welcome to Sappy Stork Vending");
			System.out.println("Please choose one of the following options");
			System.out.println("Enter number to select option");
			System.out.println("1. Display Vending Machine Items");
			System.out.println("2. Purchase");
			String userInput = input.nextLine();
        
	        if (userInput.equals("1")) {
	        	System.out.println("Vending Machine Item"+ "\t"  +  "Quantity" );
	        	System.out.println("__________________________________________" );
			    for(Item entry: vendingMachine.getItems() ){
			
			    	System.out.print(entry.getItemName() + "\t    "); 
			    	System.out.print(entry.getQuantity());
					System.out.println();
			    	}
			    
		     } else if (userInput.equals("2")) { 
		    	// while (purchasing) {
		    		 
			    	 System.out.println("Please choose one of the following options");
			         System.out.println("Enter number to select option");
			         System.out.println("1. Feed Money");
			         System.out.println("2. Select Product");
			         System.out.println("3. Finish Transaction");
			         System.out.println("Current balance " + totalMoney);
			         String userInputPurchasing = input.nextLine();
			         
			         //take away from inventory
			         //enter method to calculate current money provided
			         	if (userInputPurchasing.equals("1")) {
			         		//customer can feed money into the machine and add to totalMoneyProvided
			         		//Scanner moneyInput = new Scanner(System.in);
			    			System.out.println("Please insert 1, 2, 5, or 10 dollar bills only");
			    			Scanner moneyInput = new Scanner(System.in);
			    			String valueOfBills = input.nextLine();
			    			totalMoney = totalMoney.plus(moneyFeed(valueOfBills));
			    			
			         	} else if (userInputPurchasing.equals("2")) {
			         		
			         		//allows customer to select slotIdentifier
			         		//decrease slotIdentifier's quantity by 1
			         		System.out.println("Item Code     "+ "Item \t"  +  "Quantity" );
				        	System.out.println("__________________________________________" );
		        			for(Item entry: vendingMachine.getItems() ){
					    	System.out.print(entry.getSlotIdentifier() + "\t    ");
					    	System.out.print(entry.getItemName() + "\t    "); 
					    	System.out.print(entry.getQuantity());  
							System.out.println();
		        			}
									
						    System.out.println("Please Select Item Code");
			    			//Scanner itemInput = new Scanner(System.in);
			    			String itemCode = input.nextLine();
			    			Item userSelection = vendingMachine.getItem(itemCode);
			    			PrintWriter fileOutput = null;
			    			if (userSelection != null && userSelection.getSlotIdentifier().contains(itemCode)) {
			    				if (userSelection.getQuantity() > 0) {
			    				
					    			System.out.println("You selected "+ userSelection.getItemName() +"." );  
					    			if(totalMoney.isGreaterThanOrEqualTo(userSelection.getItemPrice()) ) {
					    				try {
					    					fileOutput = new PrintWriter(new FileOutputStream("TransactionLog.txt"));
					    					totalMoney = totalMoney.minus(userSelection.getItemPrice());
					    					System.out.println(userSelection.getItemName() +  " was dispensed and " + userSelection.getItemPrice() + " was subtracted from your account");
					    					int quantity = userSelection.reduceQuantity(1);
					    					fileOutput.println("DateTime Product Slot AmountAccepted ChangeTendered");
					    					fileOutput.print(new Date() +"  " + userSelection.getItemName()+ "  " +userSelection.getSlotIdentifier() + " "+
					    							userSelection.getItemPrice()+ totalMoney);
					    				
					    					fileOutput.flush();
					    					}catch (FileNotFoundException e) {
					    						System.out.println("Could Not Open file");
					    						System.exit(1);
					    					}finally{
					    							fileOutput.close();
					    						}
					    			} else {
					    			    System.out.println("Your don't have enough money.  Please return to the main menu and insert more money.");
					    			}
			    				} else {
			    					System.out.println("SOLD OUT");
			    				}
			    			} else {
			    				System.out.println("Not a valid item code.");
			    			}
					    			    
			         	} else if (userInputPurchasing.equals("3")) {
			         	  	
			         		int quarters = (100 * (totalMoney.getDollars()) + totalMoney.getCents()) / 25;
			         		System.out.println("Your change is " + quarters + " quarters");
			         		totalMoney = totalMoney.minus(totalMoney);
			         	} else {
			         		System.out.println("Please choose a 1, 2, or 3");
			         	}
		     }else if (userInput.equals("0")) {
		    	 PrintWriter fileSales = null;
		    	 try {
		    		 fileSales = new PrintWriter(new FileOutputStream("Vendo_Matic_Sales_DateTime.csv"));
		    		 fileSales.println("DateTime Items");
		    		 for(Item entry: vendingMachine.getItems() ){
					    	fileSales.print("Date Report was printed   " +new Date() +"   Slot     " + entry.getSlotIdentifier() + "\t   Name     ");
					    	fileSales.print(entry.getItemName() + "\t       Current Quantity  "); 
					    	fileSales.print(entry.getQuantity()+ "     ");
					    	fileSales.println("___________________");
		    		 }
		    		
		    		 fileSales.flush();
				}catch (FileNotFoundException e) {
					System.out.println("Could Not Open file");
					System.exit(1);
				}finally{
						fileSales.close();
					}
		     
			} else {
				//purchasing = false;
				System.out.println("Please choose a 1 or a 2");
			}
	
		}
    }
 
		public static DollarAmount moneyFeed(String valueOfBills) {
			int intValueOfBills = Integer.parseInt(valueOfBills);
			DollarAmount additonalBalance = new DollarAmount(0);
			if (intValueOfBills == 1) {
				//DollarAmount totalMoney = additonalBalance.plus(new DollarAmount(100));
				additonalBalance = additonalBalance.plus(new DollarAmount(100));
				}else if (intValueOfBills == 2){
					additonalBalance = additonalBalance.plus(new DollarAmount(200));	
				}else if (intValueOfBills == 5){
					additonalBalance = additonalBalance.plus(new DollarAmount(500));
				}else if (intValueOfBills == 10){
					additonalBalance = additonalBalance.plus(new DollarAmount(1000));
				}else {
					System.out.println("That is not a valid denomination. ");
				}
			return additonalBalance;
			
		}
		
		
}
			
//			
//			




//Broadly, think about what objects are involved in a vending machine:
//
//VendingMachine - possibly an abstract class
//DrinkMachine, SnackMachine, and classes extending VendingMachine
//VendingProduct - an abstract class?
//Drink, other classes extending VendingProduct
//Coke, other classes extending Drink
//&c, &c.
//But I'm sure you can figure that out pretty easily. The nuts and bolts of the machine will take place in some kind of utility class, with methods to accept bills and coins, calculate change, etc.
//
//Further reading:
//
//Here is a good article on beginning an OOP design, by Allen Holub.
//Here is the beginning of a Coffee Vending Machine design using OOP.

//Start with the major actions (putting in money, pressing a selection, receiving drink)
//Continue decomposing every action into smaller actions and responses until it becomes almost trivial. So for putting in money, you'd have to know how much was being put in, the total that was put in, the amount to be displayed, etc.
//Think of any scenarios where your actions would no longer be valid (you push a selection and the machine is empty), and how you would deal with it. (return their money, prompt for another choice, etc.)
//Assign the actions and responses to the actors and to the system. Who puts in the money, who keeps track of the running total?