package com.techelevator;

public class Item {
	protected String slotIdentifier;
	protected String itemName;
	protected DollarAmount itemPrice;
	protected int quantity;

	
	
	
	
	public Item(String slotIdentifier, String itemName, DollarAmount itemPrice, int quantity) {
		super();
		this.slotIdentifier = slotIdentifier;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.quantity = quantity;

	
	}
	public String getSlotIdentifier() {
		return slotIdentifier;
	}
	public void setSlotIdentifier(String slotIdentifier) {
		this.slotIdentifier = slotIdentifier;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public DollarAmount getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(DollarAmount itemPrice) {
		this.itemPrice = itemPrice;
	}
	public int reduceQuantity(int amountToReduce) {
		return this.quantity = quantity -amountToReduce;
	}
	public int getQuantity() {
		//this.quantity -= quantity;
		return quantity;
	}
	public String setSlotIdentifier(Item userSelection) {
		 return slotIdentifier;
		
	}
	
	
}
