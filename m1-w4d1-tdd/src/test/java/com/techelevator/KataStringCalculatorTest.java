package com.techelevator;

import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.Text;

public class KataStringCalculatorTest {
<<<<<<< HEAD
/*
 * "" -> returns 0
"1" -> returns 1
"1,2" -> returns 3
 */
	
	@Test
	//Arrange
	//Act
	//Assert
=======
//	Kata String Calculator
//	Create a simple String Calculator with a method int Add(string numbers).
//	Step 1
//	The method can take 0, 1, or 2 numbers and will return their sum. For an empty string it will return 0.
//	Sample Output
//	"" -> returns 0
//	"1" -> returns 1
//	"1,2" -> returns 3
//	Hint Begin with the simplest test case using an empty string and move to 1 then 2 numbers.
//	Step 2
//	Allow the Add method to handle an unknown amount of numbers.
//	Sample Output
//	"1,5,7" -> returns 13
//	Step 3
//	Allow the Add method to handle new lines between numbers (instead of commas)
//	Sample Output
//	"1\n2,3" -> returns 6
//	"3\n5\n2,4" -> returns 14 
//	The input "1,\n" is not valid. A comma will not end on the line. You do not need to code for it.
//	Step 4 (Bonus)
//	Support different delimters. To change a delimiter, the beginning of the string will contain a separate line that looks like "//[delimeter]\n[numbers...]"
//	Sample Output
//	//;\n1;2" -> returns 3
//	//!\n4!9" -> returns 13

	private KataStringCalculator stringCalculator;
	
	@Before
	public void setup() {
		stringCalculator = new KataStringCalculator();
	}
	
	@Test
	public void given_an_empty_string_return_zero() {
		//act
		int result = stringCalculator.add("");
		
		//assert
		Assert.assertEquals(0, result);
	}
	
	@Test
	public void given_one_return_one() {
		//act
		int result = stringCalculator.add("1");
		
		//assert
		Assert.assertEquals(1, result);
	}
	
	@Test
	public void given_one_comma_two_return_three() {
		//act
		int result = stringCalculator.add("1,2");
		
		//assert
		Assert.assertEquals(3, result);
	}
	
	@Test
	public void given_one_comma_five_comma_seven_return_thirteen() {
		//act
		int result = stringCalculator.add("1,5,7");
		
		//assert
		Assert.assertEquals(13, result);
	}
	
	@Test
	public void given_one_slash_n_two_comma_three_return_six() {
		//act
		int result = stringCalculator.add("1\n2,3");
		
		//assert
		Assert.assertEquals(6, result);
	}
	
	@Test
	public void given_three_slash_n_five_slash_n_two_comma_four_return_fourteen() {
		//act
		int result = stringCalculator.add("3\n5\n2,4");
		
		//assert
		Assert.assertEquals(14, result);
	}
	
	@Test
	public void given_bslash_bslash_semicolin__slash_n_one_semicolin_two_return_three() {
		//act
		int result = stringCalculator.add("//;\n1;2");
		
		//assert
		Assert.assertEquals(3, result);
	}
	
	@Test
	public void given_bslash_bslash_exclamation_slash_n_four_exclamation_nine_return_thirteen() {
		//act
		int result = stringCalculator.add("//!\n4!9");
		
		//assert
		Assert.assertEquals(13, result);
	}
>>>>>>> 03048599140901f8e672da9b171f02f5e721b15c
	
	
}
