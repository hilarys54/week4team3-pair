package com.techelevator;

import java.util.ArrayList;
import java.util.List;



public class KataStringCalculator {
//	Kata String Calculator
//	Create a simple String Calculator with a method int Add(string numbers).
//	Step 1
//	The method can take 0, 1, or 2 numbers and will return their sum. For an empty string it will return 0.
//	Sample Output
//	"" -> returns 0
//	"1" -> returns 1
//	"1,2" -> returns 3
//	Hint Begin with the simplest test case using an empty string and move to 1 then 2 numbers.
//	Step 2
//	Allow the Add method to handle an unknown amount of numbers.
//	Sample Output
//	"1,5,7" -> returns 13
//	Step 3
//	Allow the Add method to handle new lines between numbers (instead of commas)
//	Sample Output
//	"1\n2,3" -> returns 6
//	"3\n5\n2,4" -> returns 14 
//	The input "1,\n" is not valid. A comma will not end on the line. You do not need to code for it.
//	Step 4 (Bonus)
//	Support different delimters. To change a delimiter, the beginning of the string will contain a separate line that looks like "//[delimeter]\n[numbers...]"
//	Sample Output
//	//;\n1;2" -> returns 3
//	//!\n4!9" -> returns 13
	
	public int add(String text) {
		if (text.isEmpty()) {
			return 0;
		} else {
			int sum = 0;
			for (int i =0; i < text.length(); i++) {
				if(Character.isDigit(text.charAt(i))) {
					sum = sum +Character.getNumericValue(text.charAt(i));
					}
				}
			return sum;
		}
	

	
	
}
}
